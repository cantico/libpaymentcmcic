<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__).'/configuration.php';
require_once dirname(__FILE__).'/data.class.php';

// Interface retour

class LibPaymentCmCic_return extends LibPaymentCmCic_Data 
{
    protected $posted;
    
    
    public function __construct(LibPaymentCmCic_Configuration $configuration)
    {
        $this->_configuration = $configuration;
        
        $this->posted = $_POST;
        $this->posted['version'] = '3.0';
    }
    
    
    protected function getMacStr()
    {
        $fields = array('TPE', 'date', 'montant', 'reference', 'texte-libre', 'version', 
            'code-retour', 'cvx', 'vld', 'brand', 'status3ds', 'numauto', 'motifrefus', 
            'originecb', 'bincb', 'hpancb', 'ipclient', 'originetr', 'veres', 'pares');

	$mandatory = array_flip(array('TPE', 'date', 'montant', 'reference', 'texte-libre', 'version', 
            'code-retour', 'cvx', 'vld', 'brand', 'status3ds', 'numauto'));
        
        $str = '';
        foreach($fields as $f) {
            if (!isset($this->posted[$f]) && isset($mandatory[$f])) {
                throw new Exception('Missing posted field '.$f);
            }
            
            $value = isset($this->posted[$f]) ? $this->posted[$f] : '';
            $str .= $value.'*';
        }
        
        return $str;
    }
    
    
    /**
     * Verift that the remote addres is in the allowed IP list
     */
    public function isIpValid()
    {
        bab_debug($_SERVER['REMOTE_ADDR']);
        
        if (trim($this->_configuration->allowedResponseIp) == '') {
            return false;    
        }
            
        $allowedResponseIp = explode(',', $this->_configuration->allowedResponseIp);
        foreach($allowedResponseIp as $ip) {
            
            $ip = trim($ip);
            
            if ($ip === $_SERVER['REMOTE_ADDR']) {
                return true;
            }
        }
        
        bab_debug(LibPaymentCmCic_translate('IP address not allowed in the return interface '.$_SERVER['REMOTE_ADDR']));
        return false;
    }
    
    
    
    public function isValid()
    {
        if (!isset($this->posted['MAC'])) {
            bab_debug('Missing posted MAC value');
            return false;
        }
        
        if (strtolower($this->posted['MAC']) !== $this->getMac($this->getMacStr())) {
            bab_debug('Mac validation failed');
            return false;
        }
        
        return true;
    }
    
    public function getAmount()
    {
        return (float) substr($this->posted['montant'], 0, -3);
    }
    
    
    protected function getFilterMessage($filter)
    {
        switch($filter)
        {
            case 1: return LibPaymentCmCic_translate('IP Adress');
            case 2: return LibPaymentCmCic_translate('Card number');
            case 3: return 'BIN de carte';
            case 4: return LibPaymentCmCic_translate('Card country');
            case 5: return LibPaymentCmCic_translate('IP country');
            case 6: return LibPaymentCmCic_translate('Card country / IP country consistency');
            case 7: return LibPaymentCmCic_translate('Disposable Email');
            case 8: return LibPaymentCmCic_translate('Limited amount for a CB in a given period');
            case 9: return LibPaymentCmCic_translate('Limited transactions number for a CB in a given period');
            case 11: return LibPaymentCmCic_translate('Limited transactions number per alias in a given period');
            case 12: return LibPaymentCmCic_translate('Limited amount per alias in a given period');
            case 13: return LibPaymentCmCic_translate('Limited amount per IP in a given period');
            case 14: return LibPaymentCmCic_translate('Limited transactions number per IP in a given period');
            case 15: return LibPaymentCmCic_translate('Cards testers');
            case 16: return LibPaymentCmCic_translate('Limitation on number of CB aliases');
        }
    }
    
    
    protected function getErrorMessage()
    {
        switch($this->posted['motifrefus']) {
        
            case 'Appel Phonie':
                // translators: la banque du client demande des informations complementaires
                return LibPaymentCmCic_translate('The customer bank request additional informations');
                
            case 'Refus':
            case 'Interdit':
                // translators: la banque du client refuse d'accorder l'autorisation
                return LibPaymentCmCic_translate('The customer bank reject authorization');

                
            case 'filtrage':
                $filters = explode('-', $this->posted['filtragecause']);
                $reasons = array();
                foreach($filters as $f) {
                    $reasons[] = $this->getFilterMessage((int) trim($f));
                }
                
                // translators: la demande de paiement a �t� bloqu�e par le param�trage de filtrage que le commer�ant a mis en place dans son Module Pr�vention Fraude
                $message = LibPaymentCmCic_translate('The payment request was blocked by the filter settings that the trader has established in its Fraud Prevention Module.');
                
                if ($reasons) {
                    $message .= ' '.sprintf(LibPaymentCmCic_translate('Filters involved: %s'), implode(', ', $reasons));
                }
                
                return $message;
                
            case 'scoring':
                // translators: la demande de paiement a �t� bloqu�e par le param�trage de scoring que le commer�ant a mis en place dans son Module Pr�vention Fraude
                return LibPaymentCmCic_translate('The payment request was blocked by scoring setup that the trader has established in its Fraud Prevention Module');
                
            case '3DSecure':
                // translators: si le refus est lie � une authentification 3DSecure negative re�ue de la banque du porteur
                return LibPaymentCmCic_translate('The customer bank reject the 3DSecure authentication');
        
        }
    }
    
    
    protected function getPaymentMean()
    {
        switch($this->posted['brand']) {
            case 'AM': return 'American Express';
            case 'CB': return 'GIE CB';
            case 'MC': return 'Mastercard';
            case 'VI': return 'Visa';
        }
        
        return '';
    }
    
    
    
    protected function getTransactionDate()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/dateTime.php';
        $m = null;
        if (preg_match('/(\d{2})\/(\d{2})\/(\d{4}) a (\d{2}):(\d{2}):(\d{2})/', $this->posted['Date'], $m)) {
            return new BAB_DateTime($m[3], $m[2], $m[1], $m[4], $m[5], $m[6]);
        }
        
        return null;
    }
    
    
    public function updatePayment()
    {
        /* @var $CmCic Func_Payment_CmCic */
        $CmCic = bab_functionality::get('Payment/CmCic');
        
        $token = $this->posted['reference'];
        
        $paymentLogSet = new payment_logSet();
        $paymentLog = $paymentLogSet->get($paymentLogSet->token->is($token));
        
        if (!$paymentLog) {
            bab_debug(sprintf('Received payment response for non existing payment token (%s)', $token));
            return;
        }
        
        $paymentLog->response = serialize($this->posted);
        $paymentLog->save();
        
        $payment = unserialize($paymentLog->payment);
        
        bab_debug($payment);
        
        $code = $this->posted['code-retour'];
        
        if ('paiement' === $code || 'payetest' === $code) {
        
        
            $paymentEvent = $CmCic->newEventPaymentSuccess();
            
            $paymentEvent->setPayment($payment);
            $paymentEvent->setResponseAmount($this->getAmount());
            $paymentEvent->setResponseAuthorization($this->posted['numauto']);
            $paymentEvent->setResponseTransaction('');
            
            
        
        } else {
            
            // $code === 'Annulation'
            // ou paiement fractionne
            
            $paymentEvent = $CmCic->newEventPaymentError();
            
            $paymentEvent->setPayment($payment);
            $paymentEvent->setResponseAmount($this->getAmount());

            $paymentEvent->errorCode = 0;
            $paymentEvent->errorMessage = $this->getErrorMessage();
            $paymentEvent->response_code = $this->posted['motifrefus'];
            $paymentEvent->payment_means = $this->getPaymentMean();
            
            if (isset($this->posted['hpancb'])) {
                // Hachage irr�versible (HMAC-SHA1) du num�ro de la carte de cr�dit utilis�e pour effectuer le paiement (identifiant de
                // mani�re unique une carte de cr�dit pour un commer�ant donn�) Uniquement en cas de souscription du module pr�vention fraude
                $paymentEvent->card_number = $this->posted['hpancb'];
            }
            
            $paymentEvent->transmission_date = $this->getTransactionDate();
        }
        
        bab_fireEvent($paymentEvent);
        
    }
}


bab_debug('CM-CIC Interface retour '.$_SERVER['REMOTE_ADDR']);


$configuration = LibPaymentCmCic_getConfiguration();

$returnIface = new LibPaymentCmCic_return($configuration);

/*
if (!$returnIface->isIpValid()) {
    die("version=2\ncdr=1\n");
}
*/

if (!$returnIface->isValid()) {
    die("version=2\ncdr=1\n");
}

// notify application about the payment
$returnIface->updatePayment();

die("version=2\ncdr=0\n");