<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */



class LibPaymentCmCic_Data
{
    
    /**
     * @var LibPaymentCmCic_Configuration
     */
    protected $_configuration;
    
    
    /**
     * Return the key to be used in the hmac function
     * @return string
     */
    protected function getUsableKey()
    {
    
        $hexStrKey  = substr($this->_configuration->secureKey, 0, 38);
        $hexFinal   = "" . substr($this->_configuration->secureKey, 38, 2) . "00";
    
        $cca0=ord($hexFinal);
    
        if ($cca0>70 && $cca0<97)
            $hexStrKey .= chr($cca0-23) . substr($hexFinal, 1, 1);
        else {
            if (substr($hexFinal, 1, 1)=="M")
                $hexStrKey .= substr($hexFinal, 0, 1) . "0";
            else
                $hexStrKey .= substr($hexFinal, 0, 2);
        }
    
    
        return pack("H*", $hexStrKey);
    }
    
    
    protected function getMac($macStr)
    {

        // RFC2104 with SHA1 or MD5 allowed
        
        $secureKey = $this->getUsableKey();
        
        return strtolower(hash_hmac("sha1", $macStr, $secureKey));
    }
    
}