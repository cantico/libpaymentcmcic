; <?php/*
[general]
name                          = "LibPaymentCmCic"
version                       = "0.0.2"
addon_type                    = "LIBRARY"
mysql_character_set_database  = "latin1,utf8"
encoding                      = "UTF-8"
description                   = "Credit Mutuel and CIC Payment Gateway functionality"
description.fr                = "Fonctionnalité de passerelle de paiement vers le Crédit Mutuel et le CIC"
delete                        = "1"
longdesc                      = ""
ov_version                    = "8.1.98"
php_version                   = "5.2.0"
addon_access_control          = "0"
configuration_page            = "systemconf"
db_prefix                     = "libpaymentcmcic"
author                        = "Cantico ( support@cantico.fr )"
icon                          = "credit_card1.png"
tags						  ="library,payment"

[addons]
widgets						  = ">=1.0.10"
LibTranslate                  = ">=1.12.0rc3.01"

;*/ ?>