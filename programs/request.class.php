<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__).'/data.class.php';

/**
 * Payment request form variables
 */
class LibPaymentCmCic_PaymentRequest extends LibPaymentCmCic_Data
{

    
    
    protected $version = '3.0';
    
    protected $TPE;
    
    protected $date;
    
    protected $montant;
    
    protected $reference;
    
    protected $MAC;
    
    protected $url_retour;
    
    protected $url_retour_ok;
    
    protected $url_retour_err;
    
    protected $lgue;
    
    protected $societe;
    
    protected $mail;
    
    protected $options;
    
    
    protected $_text;
    
    public function __construct()
    {
        $this->date = date('d/m/Y:H:i:s');
        
        $this->lgue = mb_strtoupper(bab_getLanguage());
        
        
    }
    
    
    public function setConfiguration(LibPaymentCmCic_Configuration $configuration)
    {
        $this->_configuration = $configuration;
        $this->TPE = $configuration->TPE;
        $this->societe = $configuration->societe;
    }
    
    
    public function setPayment(libpayment_Payment $payment)
    {
        $this->montant = $payment->getAmount().$payment->getCurrency();
        $this->reference = $payment->getToken();
        $this->mail = $payment->getEmail();
        
        $this->url_retour = $GLOBALS['babUrlScript'].'?addon=LibPaymentCmCic.userreturn&token='.$payment->getToken();
        $this->url_retour_ok = $this->url_retour.'&status=ok';
        $this->url_retour_err = $this->url_retour.'&status=err';
        
        $this->_text = $payment->getText();
    }
    
    
    
    protected function htmlEncode($data)
    {
        $SAFE_OUT_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890._-";
        $encoded_data = "";
        $result = "";
        for ($i=0; $i<strlen($data); $i++)
        {
            if (strchr($SAFE_OUT_CHARS, $data{$i})) {
                $result .= $data{$i};
            }
            else if (($var = bin2hex(substr($data,$i,1))) <= "7F"){
                $result .= "&#x" . $var . ";";
            }
            else
                $result .= $data{$i};
    
        }
        return $result;
    }
    
    
    

    
    
    
    
    protected function setMac()
    {
        $macProp = array('TPE', 'date', 'montant', 'reference', 'texte-libre', 'version', 'lgue', 'societe', 'mail',   
        'nbrech',                       // nombre d'echeance pour cette commande
            'dateech1', 'montantech1', 
            'dateech2', 'montantech2', 
            'dateech3', 'montantech3', 
            'dateech4', 'montantech4'
            , 'options');
        

        $values = array();
        foreach($macProp as $name) {
            
            
            // ignorer les champs du paiement fractionne
            
            switch($name) {
                case 'texte-libre':
                    $values[] = $this->_text;
                    continue 2;
                    
                    
                case 'nbrech':
                case 'dateech1':
                case 'dateech2':
                case 'dateech3':
                case 'dateech4':
                case 'montantech1':
                case 'montantech2':
                case 'montantech3':
                case 'montantech4':
                case 'options':
                    $values[] = '';
                    continue 2;
            }
            
            if (empty($this->$name)) {
                throw new Exception(sprintf(LibPaymentCmCic_translate('Missing field "%s"', $name)));
            }

            
            $values[] = (string) $this->$name;
        }

        $macStr = implode('*', $values);
        
        $this->MAC = $this->getMac($macStr);
    }
    
    
    public function getHtml()
    {
        $this->setMac();
        
        $form = '<form style="text-align:center" method="post" action="'.bab_toHtml($this->_configuration->cgiurl).'">'."\n";
        
        
        
        $vars = get_object_vars($this);
        foreach($vars as $name => $value) {
            if ('_' === mb_substr($name, 0 ,1)) {
                continue;
            }
            
            $form .= '<input type="hidden" name="'.bab_toHtml($name).'" value="'.bab_toHtml($value).'" />'."\n";
        }
        
        $form .= '<input type="hidden" name="texte-libre" value="'.$this->htmlEncode($this->_text).'" />'."\n";
        
        $form .= '
        <input type="hidden" name="nbrech"           value="" />
        <input type="hidden" name="dateech1"         value="" />
        <input type="hidden" name="montantech1"      value="" />
        <input type="hidden" name="dateech2"         value="" />
        <input type="hidden" name="montantech2"      value="" />
        <input type="hidden" name="dateech3"         value="" />
        <input type="hidden" name="montantech3"      value="" />
        <input type="hidden" name="dateech4"         value="" />
        <input type="hidden" name="montantech4"      value="" />
        ';
        
        $addon = bab_getAddonInfosInstance('LibPaymentCmCic');
        $form .= '<img src="'.bab_toHtml($addon->getImagesPath().'logocmcicpaiement.gif').'" alt="CM-CIC Paiement" /><br /><br />'."\n";
        
        // Add the submit button
        
        $form .= '<input type="submit" name="bouton" value="'.LibPaymentCmCic_translate('Pay by credit card').'" />';
        
        $form .= '</form>'."\n";
        
        return $form;
    }
}