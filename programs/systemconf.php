<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/configuration.php';


/* @var $I Func_Icons */
$I = bab_functionality::get('Icons');
$I->includeCss();




/**
 *
 * @return Widget_Frame
 */
function LibPaymentCmCic_TpeEditor()
{
	$W = bab_Widgets();
	$editor = $W->Frame();


	$layout = $W->VBoxItems(

		$W->LabelledWidget(
			LibPaymentCmCic_translate('Name'),
			$W->LineEdit()
				->setMandatory(true, LibPaymentCmCic_translate('You must specify a unique name for this TPE.'))
				->addClass('widget-fullwidth')
				->setName('name')
		),
		$W->LabelledWidget(
			LibPaymentCmCic_translate('Description'),
			$W->TextEdit()
				->setLines(2)
				->addClass('widget-fullwidth')
				->setName('description')
		),
		$W->LabelledWidget(
			LibPaymentCmCic_translate('Virtual TPE number of the merchant provided by the bank'),
			$W->LineEdit()->setMandatory(true, LibPaymentCmCic_translate('You must specify a TPE number.')),
			'TPE'
		),
		$W->LabelledWidget(
			LibPaymentCmCic_translate('Societe'),
			$W->LineEdit(),
			'societe',
		    LibPaymentCmCic_translate('Alphanumeric code enabling the merchant to use the same Virtual TPE for different sites')
		),

		$W->LabelledWidget(
			LibPaymentCmCic_translate('CGI url'),
			$cgiurl = $W->SuggestLineEdit(),
		    'cgiurl',
		    LibPaymentCmCic_translate('Url to the bank payment server')
		),
	    
	    $W->LabelledWidget(
	        LibPaymentCmCic_translate('Security key'),
	        $W->LineEdit()
	           ->setMandatory(true, LibPaymentCmCic_translate('You must specify the security key.'))
	           ->addClass('widget-fullwidth'),
	        'secureKey'
	    )
	    /*
	    ,

		$W->LabelledWidget(
			LibPaymentCmCic_translate('Allowed response IP on return interface (comma separated list)'),
			$W->LineEdit()
				->addClass('widget-fullwidth')
				->setSize(80)
				->setName('allowedResponseIp')

		) */


	)->setVerticalSpacing(1, 'em');

	$editor->setLayout($layout);
	
	
	$cgiurl
	   ->setMinChars(1)
	   ->setMandatory(true, LibPaymentCmCic_translate('You must specify a CGI url.'))
	   ->addClass('widget-fullwidth')
	   ->setSize(80);
	
	
	if ($keyword = $cgiurl->getSearchKeyword()) {
	    $cgiurl->addSuggestion('', 'https://paiement.creditmutuel.fr/test/paiement.cgi', LibPaymentCmCic_translate('Credit mutuel test environement'));
	    $cgiurl->addSuggestion('', 'https://ssl.paiement.cic-banques.fr/test/paiement.cgi', LibPaymentCmCic_translate('CIC test environement'));
	    $cgiurl->addSuggestion('', 'https://ssl.paiement.banque-obc.fr/test/paiement.cgi', LibPaymentCmCic_translate('OBC test environement'));
	    $cgiurl->addSuggestion('', 'https://paiement.creditmutuel.fr/paiement.cgi', LibPaymentCmCic_translate('Credit mutuel production environement'));
	    $cgiurl->addSuggestion('', 'https://ssl.paiement.cic-banques.fr/paiement.cgi', LibPaymentCmCic_translate('CIC production environement'));
	    $cgiurl->addSuggestion('', 'https://ssl.paiement.banque-obc.fr/paiement.cgi', LibPaymentCmCic_translate('OBC production environement'));
	    $cgiurl->sendSuggestions();
	}

	return $editor;
}



/**
 *
 * @param string $tpe
 */
function LibPaymentCmCic_editTpe($tpe = null)
{
	$W = bab_Widgets();

	$addon = bab_getAddonInfosInstance('LibPaymentCmCic');
	$addonUrl = $addon->getUrl();


	$page = $W->babPage();
	$page->setLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));



	$form = $W->Form();
	$form->addClass('BabLoginMenuBackground');
	$form->setLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));
	$form->setName('tpe');

	$editor = LibPaymentCmCic_TpeEditor();

	$form->addItem($editor);

	$form->addItem(
		$W->FlowItems(
			$W->SubmitButton()
				->setLabel(LibPaymentCmCic_translate('Save configuration'))
				->addClass('icon ' . Func_Icons::ACTIONS_DIALOG_OK),
			$W->Link(
				LibPaymentCmCic_translate('Cancel'),
				$addonUrl . 'systemconf&idx=displayTpeList'
			)->addClass('icon ' . Func_Icons::ACTIONS_DIALOG_CANCEL)
		)
		->setSpacing(1, 'em')
		->addClass(Func_Icons::ICON_LEFT_16)
	);

	$form->setHiddenValue('tg', bab_rp('tg'));
	$form->setHiddenValue('idx', 'saveTpe');

	if (isset($tpe)) {
		$title = LibPaymentCmCic_translate('Edit TPE configuration');

		$configuration = LibPaymentCmCic_getConfiguration($tpe);
		
		$tpe = array();
		foreach($configuration as $key => $value) {
		    $tpe[$key] = $value;
		}
		
		$form->setValues(
			array(
				'tpe' => $tpe
			)
		);
		$form->setHiddenValue('tpe[originalName]', $tpe['name']);
	} else {
		$title = LibPaymentCmCic_translate('New TPE configuration');
	}

	$page->setTitle($title);

	$page->addItem($form);

	$page->displayHtml();
}




function LibPaymentCmCic_saveTpe(Array $tpe)
{
	$configuration = new LibPaymentCmCic_Configuration();
	
	foreach($configuration as $key => $value) {
	    if (!property_exists($configuration, $key)) {
	        continue;
	    }
	    $configuration->$key = $tpe[$key];
	}

	if (isset($tpe['originalName']) && $tpe['name'] != $tpe['originalName']) {
		LibPaymentCmCic_renameConfiguration($tpe['originalName'], $tpe['name']);
	}

	LibPaymentCmCic_saveConfiguration($tpe['name'], $configuration);
}




function LibPaymentCmCic_displayTpeList()
{
	$W = bab_Widgets();

	$page = $W->babPage();
	$page->setLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));

	$page->addItem($W->Title(LibPaymentCmCic_translate('List of configured TPE')));

	$tpeNames = LibPaymentCmCic_getConfigurationNames();


	$tableView = $W->TableView();


	$addon = bab_getAddonInfosInstance('LibPaymentCmCic');
	$addonUrl = $addon->getUrl();

	$tableView->addItem(
		$W->Label(LibPaymentCmCic_translate('Name')),
		0, 0
	);
	$tableView->addItem(
		$W->Label(LibPaymentCmCic_translate('TPE number')),
		0, 1
	);

	$tableView->addItem(
		$W->Label(''),
		0, 2
	);

	$tableView->addColumnClass(4, 'widget-column-thin');

	$tableView->addSection('tpe');
	$tableView->setCurrentSection('tpe');


	$defaultName = LibPaymentCmCic_getDefaultConfigurationName();

	$row = 0;
	foreach ($tpeNames as $name) {

		$configuration = LibPaymentCmCic_getConfiguration($name);

		$nameLabel = $W->Label($configuration->name);
		if ($name == $defaultName) {
			$nameLabel->addClass('icon ' . Func_Icons::ACTIONS_DIALOG_OK);
		}

		$tableView->addItem(
			$W->VBoxItems(
				$nameLabel,
				$W->Label($configuration->description)->addClass('widget-small')
			)->addClass(Func_Icons::ICON_LEFT_16),
			$row, 0
		);
		$tableView->addItem(
			$W->Label($configuration->TPE),
			$row, 1
		);

		$tableView->addItem(
			$W->FlowItems(
				$W->Link(
					LibPaymentCmCic_translate('Edit'),
					$addonUrl . 'systemconf&idx=editTpe&tpe=' . $name
				)->addClass('icon widget-nowrap ' . Func_Icons::ACTIONS_DOCUMENT_EDIT),

				$W->Link(
					LibPaymentCmCic_translate('Set default'),
					$addonUrl . 'systemconf&idx=setDefaultTpe&tpe=' . $name
				)->addClass('icon widget-nowrap ' . Func_Icons::ACTIONS_DIALOG_OK),

				$W->Link(
						LibPaymentCmCic_translate('Delete'),
						$addonUrl . 'systemconf&idx=deleteTpe&tpe=' . $name
				)->setConfirmationMessage(sprintf(LibPaymentCmCic_translate('Are you sure you want to delete the TPE \'%s\'?'), $name))
				->addClass('icon widget-nowrap ' . Func_Icons::ACTIONS_EDIT_DELETE)
			)
			->setSpacing(4, 'px')
			->addClass(Func_Icons::ICON_LEFT_16),
			$row, 2
		);

		$row++;
	}

	$page->addItem($tableView);

	$page->addItem(
		$W->FlowItems(
			$W->Link(
				LibPaymentCmCic_translate('Add configuration'),
				$addonUrl . 'systemconf&idx=editTpe'
			)->addClass('icon ' . Func_Icons::ACTIONS_LIST_ADD)
		)->addClass(Func_Icons::ICON_LEFT_16)
	);
	
	$urlOk = sprintf(LibPaymentCmCic_translate('Url Ok: %s'), $GLOBALS['babUrlScript'].'?addon=libpaymentcmcic.return');
	$urlNotOk = sprintf(LibPaymentCmCic_translate('Url not Ok: %s'), $GLOBALS['babUrlScript'].'?addon=libpaymentcmcic.return&error=1');
	
	$page->addItem($W->Section(LibPaymentCmCic_translate('Return interface url'),
	    $W->VBoxItems(
	        $W->Label($urlOk),
	        $W->Label($urlNotOk)
	    )
	));

	$page->displayHtml();
}




function LibPaymentCmCic_deleteTpe($tpe)
{
	LibPaymentCmCic_deleteConfiguration($tpe);
}



function LibPaymentCmCic_setDefaultTpe($tpe)
{
	LibPaymentCmCic_setDefaultConfigurationName($tpe);
}


/* main */

if (!bab_isUserAdministrator())
{
	return;
}



$idx= bab_rp('idx', 'displayTpeList');

$addon = bab_getAddonInfosInstance('LibPaymentCmCic');


switch ($idx)
{
	case 'displayTpeList':
		$babBody->addItemMenu('list', LibPaymentCmCic_translate('List'), $GLOBALS['babAddonUrl'] . 'systemconf&idx=displayTpeList');
		LibPaymentCmCic_displayTpeList();
		break;

	case 'editTpe':
		$tpe = bab_rp('tpe', null);
		$editor = LibPaymentCmCic_editTpe($tpe);
		break;

	case 'saveTpe':
		$tpe = bab_rp('tpe', null);
		$editor = LibPaymentCmCic_saveTpe($tpe);
		header('location:'. $addon->getUrl() .'systemconf&idx=displayTpeList');
		break;

	case 'deleteTpe':
		$tpe = bab_rp('tpe', null);
		LibPaymentCmCic_deleteTpe($tpe);
		header('location:'. $addon->getUrl() .'systemconf&idx=displayTpeList');
		break;

	case 'setDefaultTpe':
		$tpe = bab_rp('tpe', null);
		LibPaymentCmCic_setDefaultTpe($tpe);
		header('location:'. $addon->getUrl() .'systemconf&idx=displayTpeList');
		break;
}
