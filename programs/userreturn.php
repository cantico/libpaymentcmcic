<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2012 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/functions.php';

$token = bab_rp('token');
$status = bab_rp('status', null);

/* @var $CmCic Func_Payment_CmCic */
$CmCic = bab_Functionality::get('Payment/CmCic');

$paymentLogSet = new payment_logSet();
$paymentLog = $paymentLogSet->get($paymentLogSet->token->is($token));

if (!$paymentLog) {
    bab_debug(sprintf('Received payment response for non existing payment token (%s)', $token));
    return;
}


$payment = unserialize($paymentLog->payment);
/*@var payment_log */

if (!isset($status)) {
    
    // no payment status, page bottom link
    
    $event = $CmCic->newEventPaymentCancel();
    $event->setPayment($payment);
    bab_fireEvent($event);
    die();
}


switch($status) {
    
    case 'ok':
        $event = $CmCic->newEventPaymentUserReturn();
        break;
    case 'err':
        $event = $CmCic->newEventPaymentCancel();
        break;
        
    default:
        die('Wrong status');
}

$event->setPayment($payment);
bab_fireEvent($event);

die();
