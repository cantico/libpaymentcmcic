<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/functions.php';




class LibPaymentCmCic_Configuration {

	/**
	 * @var string Name of this configuration.
	 */
	public $name;

	/**
	 * @var string Optional description of this configuration.
	 */
	public $description;

    /**
     * Virtual TPE number
     */
	public $TPE;

	/**
	 * En Test
     *
     * Pour les banques et federations du Credit Mutuel :
     *   https://paiement.creditmutuel.fr/test/paiement.cgi
     * Pour les banques du Groupe CIC :
     *   https://ssl.paiement.cic-banques.fr/test/paiement.cgi
     * Pour la banque OBC :
     *   https://ssl.paiement.banque-obc.fr/test/paiement.cgi
     *   
     * En Production
     * 
     * Pour les banques et federations du Credit Mutuel :
     *   https://paiement.creditmutuel.fr/paiement.cgi
     * Pour les banques du Groupe CIC :
     *   https://ssl.paiement.cic-banques.fr/paiement.cgi
     * Pour la banque OBC :
     *   https://ssl.paiement.banque-obc.fr/paiement.cgi
	 */
	public $cgiurl;
	
	/**
	 * Hexadecimal secure key (version non operationelle)
	 * @var string
	 */
	public $secureKey;

	/**
	 * Code alphanumerique permettant au commer�ant d'utiliser le m�me TPE Virtuel
     * pour des sites differents (parametrages distincts) se rapportant � la meme activite
	 */
	public $societe;

	
	/**
	 * @var string
	 */
	public $allowedResponseIp;
}




/**
 *
 * @return bab_registry
 */
function LibPaymentCmCic_getRegistry()
{
	$registry = bab_getRegistryInstance();

	$registry->changeDirectory('/LibPaymentCmCic');

	return $registry;
}



/**
 * Returns the names of all configurations.
 *
 * @return array
 */
function LibPaymentCmCic_getConfigurationNames()
{
	$registry = LibPaymentCmCic_getRegistry();

	$registry->changeDirectory('configurations');

	$names = array();

	while ($name = $registry->fetchChildDir()) {
		$name = substr($name, 0, -1);
		$names[$name] = $name;
	}

	return $names;
}





/**
 * Returns all configuration information in a LibPaymentCmCic_Configuration object.
 *
 * @param string $name		The configuration name.
 *
 * @return LibPaymentCmCic_Configuration
 */
function LibPaymentCmCic_getConfiguration($name = null)
{
	$registry = LibPaymentCmCic_getRegistry();

	if (!isset($name)) {
		$name = LibPaymentCmCic_getDefaultConfigurationName();
	}

	$registry->changeDirectory('configurations');
	$registry->changeDirectory($name);

	$configuration = new LibPaymentCmCic_Configuration();
	$properties = 0;

	while ($key = $registry->fetchChildKey()) {
		$configuration->$key = $registry->getValue($key);
		$properties++;
	}
	
	if (!$properties) {
	    throw new Exception(sprintf(LibPaymentCmCic_translate('The selected configuration "%s" does not contain informations'), $name));
	}

	return $configuration;
}





/**
 * Sets the default configuration name.
 *
 * @param string $name		The configuration name.
 *
 * @return void
 */
function LibPaymentCmCic_setDefaultConfigurationName($name)
{
	$registry = LibPaymentCmCic_getRegistry();
	$identifier = $registry->setKeyValue('default', $name);
}





/**
 * Returns the default configuration name.
 *
 * @return string
 */
function LibPaymentCmCic_getDefaultConfigurationName()
{
	$registry = LibPaymentCmCic_getRegistry();
	return $registry->getValue('default');
}





/**
 * Renames a configuration.
 *
 * @param string $originalName
 * @param string $newName
 *
 * @return bool
 */
function LibPaymentCmCic_renameConfiguration($originalName, $newName)
{
	$registry = LibPaymentCmCic_getRegistry();

	$registry->changeDirectory('configurations');

	return $registry->moveDirectory($originalName, $newName);
}


/**
 * Sets all configuration information.
 *
 * @param string name The name of the configuration to save.
 * @param LibPaymentCmCic_Configuration $configuration
 *
 * @return void
 */
function LibPaymentCmCic_saveConfiguration($name, LibPaymentCmCic_Configuration $configuration)
{
	$registry = LibPaymentCmCic_getRegistry();

	$registry->changeDirectory('configurations');
	$registry->changeDirectory($name);
	
	foreach($configuration as $key => $value) {
	    
	    $registry->setKeyValue($key, $value);
	}

}





/**
 * Deletes the specified configuration.

 * @param string name The name of the configuration to delete.
 * @return bool
 */
function LibPaymentCmCic_deleteConfiguration($name)
{
	$registry = LibPaymentCmCic_getRegistry();
	$registry->changeDirectory('configurations');

	if (!$registry->isDirectory($name)) {
		return false;
	}

	$registry->changeDirectory($name);

	$registry->deleteDirectory();

	return true;
}
